<?php
namespace  System\Http;
class  Request
{
    public $data = array();
    public function __construct()
    {
        $this->data = $_REQUEST;
    }

    public function all()
    {
        return $this->data;
    }

    public function get(string $key)
    {
        if(in_array($key,array_keys($this->data)))
        {
            return $this->data[$key];
        }
    }
}