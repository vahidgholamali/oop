<?php
namespace System\Router;

use System\Http\Request;

class Router
{
    protected static $instance;

    public static function instance()
    {
        if(is_null(self::$instance))
        {
            self::$instance = new self();
        }
        return self::$instance;
    }
    private function __construct()
    {
        $this->boot();
    }
    private function getCurrentRoute()
    {

        return strtok($_SERVER['REQUEST_URI'],'?') ;
    }
    private function checkRouteExist($route)
    {
        $routes = include "routes.php";
  
        if(in_array($route,array_keys($routes)))
        {
            $routeDetails = $routes[$route];
            $middleware = $routeDetails['middleware'];
            if(isset($middleware) && !empty($middleware))
            {
             //   $middlewareInstance = new $middleware;
            }
            list($controller,$method) = explode('@',$routeDetails['controller']);
            $controllerClass = 'Application\\Controllers\\'.$controller;

            $controllerInstance = new $controllerClass;
            $request = new Request();
       
            $controllerInstance->{$method}($request);
            exit;
        }
    }
    private function boot()
    {
        $route = $this->getCurrentRoute();
        $this->checkRouteExist($route);
    }

    public function __clone()
    {
        exit;
    }
}