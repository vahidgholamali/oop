<?php
return [
    '/oop/' => 'HomeController@index',
    '/oop/admin/users/'  => [
        'controller' => 'Admin\\UsersController@index',
        'middleware'  => 'auth|download'
    ]
];