<?php
///========main
error_reporting(E_ALL);
ini_set('display_errors',1);
spl_autoload_register('autoload');
function autoload($className)
{
    $classFilePath = str_replace('\\',DIRECTORY_SEPARATOR,$className);
    $classFilePath.='.php';
    if(file_exists($classFilePath) && is_readable($classFilePath))
    {
        include_once $classFilePath;
    }
}  //=======
 $router = \System\Router\Router::instance();